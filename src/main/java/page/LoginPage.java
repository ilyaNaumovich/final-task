package page;

import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;
import static helpers.Locators.get;
import static helpers.Other.createRandomString;

/**
 * Created by User on 08.09.2016.
 */
public class LoginPage {
    private static final By EMAIL_ADDRESS_INPUT_BOX = get("LogInPage.EmailAddressInputBox");
    private static final By CREATE_ACCOUNT_BUTTON = get("LogInPage.CreateAccountButton");


    public static void fillEmailAddress(String email) {
        String newEmail = createRandomString(7) + email;
        $(EMAIL_ADDRESS_INPUT_BOX).setValue(newEmail);
    }

    public static void createAccount(){
        $(CREATE_ACCOUNT_BUTTON).click();
    }
}
