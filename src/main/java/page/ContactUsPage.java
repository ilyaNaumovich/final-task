package page;

import base.PageBase;
import com.codeborne.selenide.Condition;
import com.codeborne.selenide.WebDriverRunner;
import helpers.Waiter;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.sikuli.script.*;

import java.awt.*;

import static com.codeborne.selenide.Selenide.$;
import static helpers.Locators.get;

public class ContactUsPage extends PageBase {
    private static final By SUBJECT_HEADING_COMBO_BOX = get("ContactUsPage.SubjectHeadingComboBox");
    private static final By SUBJECT_HEADING_COMBO_BOX_CHOOSE = get("ContactUsPage.SubjectHeadingComboBox.Choose");
    private static final By SUBJECT_HEADING_COMBO_BOX_CUSTOMER_SERVICE = get("ContactUsPage.SubjectHeadingComboBox.CustomerService");
    private static final By SUBJECT_HEADING_COMBO_BOX_WEBMASTER = get("ContactUsPage.SubjectHeadingComboBox.Webmaster");

    private static final By EMAIL_ADDRESS_INPUT_BOX = get("ContactUsPage.EmailAddressInputBox");
    private static final By ORDER_REFERENCE_INPUT_BOX = get("ContactUsPage.OrderReferenceInputBox");
    private static final By ATTACH_FILE_BUTTON = get("ContactUsPage.AttachFileButton");
    private static final By MESSAGE_TEXT_FIELD = get("ContactUsPage.MessageTextField");
    private static final By SEND_BUTTON = get("ContactUsPage.SendButton");
    private static final By FILE_NAME_INPUT = get("ContactUsPage.FileNameInput");
    private static final By SUCCESS_MESSAGE_SENT = get("ContactUsPage.SuccessFileSent");
    private static final By ERROR_MESSAGE_SENT_TEXT_BOX = get("ContactUsPage.ErrorMessageTextBox");

    public static void choseWebmasterSubject() {
        $(SUBJECT_HEADING_COMBO_BOX).waitUntil(Condition.appear, 1000).click();
        $(SUBJECT_HEADING_COMBO_BOX_WEBMASTER).click();
    }

    public static void choseCustomerServiceSubject() {
        $(SUBJECT_HEADING_COMBO_BOX).waitUntil(Condition.appear, 1000).click();
        $(SUBJECT_HEADING_COMBO_BOX_CUSTOMER_SERVICE).click();
    }

    public static void fillEmailAddress(String email) {
        $(EMAIL_ADDRESS_INPUT_BOX).setValue(email);
    }

    public static void fillOrderReference(String orderReference) {
        $(ORDER_REFERENCE_INPUT_BOX).setValue(orderReference);
    }

    public static void fillMessage(String message) {
        $(MESSAGE_TEXT_FIELD).setValue(message);
    }

    public static void sendMessage() {
        $(SEND_BUTTON).click();
    }

    public static void attachFile(String fileName) throws AWTException, FindFailed, InterruptedException {
        WebDriver driver = WebDriverRunner.getWebDriver();
        WebElement webElement = driver.findElement(FILE_NAME_INPUT);
        webElement.sendKeys(fileName);
    }

    public static boolean successFileSentMessageIsVisible(){
        return $(SUCCESS_MESSAGE_SENT).isDisplayed();
    }

    public static String getErrorText(){
        return $(ERROR_MESSAGE_SENT_TEXT_BOX).getText();
    }

}
