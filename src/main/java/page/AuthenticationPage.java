package page;
import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import com.codeborne.selenide.WebDriverRunner;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;
import static helpers.Locators.get;

public class AuthenticationPage {
    private static final By PAGE_HEADING = get("AuthenticationPage.PageHeading");
    private static final By MR_RADIO_BUTTON = get("AuthenticationPage.MrRadioButton");
    private static final By MRS_RADIO_BOTTON = get("AuthenticationPage.MrsRadioButton");
    private static final By FIRST_NAME_INPUT_BOX = get("AuthenticationPage.FirstNameInputBox");
    private static final By LAST_NAME_INPUT_BOX = get("AuthenticationPage.LastNameInputBox");
    private static final By EMAIL_INPUT_BOX = get("AuthenticationPage.EmailInputBox");
    private static final By PASSWORD_INPUT_BOX = get("AuthenticationPage.PasswordInputBox");
    private static final By DAY_OF_BIRTH_INPUT_BOX = get("AuthenticationPage.DayOfBirthInput");
    private static final By DAY_OF_BIRTH_INPUT_FIELD = get("AuthenticationPage.DayOfBirthField");
    private static final By MONTH_OF_BIRTH_INPUT_BOX = get("AuthenticationPage.MonthOfBirthInput");
    private static final By YEAR_OF_BIRTH_INPUT_BOX = get("AuthenticationPage.YearOfBirthInput");
    private static final By NEWSLETTER_CHECK_BOX = get("AuthenticationPage.NewsLetterCheckBox");
    private static final By SPECIAL_OFFER_CHECK_BOX = get("AuthenticationPage.SpecialOffersCheckBox");
    private static final By ADDRESS_FIRST_NAME_INPUT_BOX = get("AuthenticationPage.AddressFirstNameInputBox");
    private static final By ADDRESS_LAST_NAME_INPUT_BOX = get("AuthenticationPage.AddressLastNameInputBox");
    private static final By COMPANY_NAME_INPUT_BOX = get("AuthenticationPage.CompanyNameInputName");
    private static final By ADDRESS_FIRST_LINE_INPUT_BOX = get("AuthenticationPage.AddressFirstLineInputBox");
    private static final By ADDRESS_SECOND_LINE_INPUT_BOX = get("AuthenticationPage.AddressSecondLineInputBox");
    private static final By CITY_INPUT_BOX = get("AuthenticationPage.CityInputBox");
    private static final By STATE_INPUT_BOX = get("AuthenticationPage.StateInputBox");
    private static final By POST_CODE_INPUT_BOX = get("AuthenticationPage.PostCodeInputBox");
    private static final By COUNTRY_INPUT_BOX = get("AuthenticationPage.Country");
    private static final By ADDITIONAL_INFORMATION_INPUT_BOX = get("AuthenticationPage.AdditionalInformationInputBox");
    private static final By HOME_PHONE_INPUT_BOX = get("AuthenticationPage.HomePhoneInputBox");
    private static final By MOBILE_PHONE_INPUT_BOX = get("AuthenticationPage.MobilePhoneInputBox");
    private static final By ALIAS_INPUT_BOX = get("AuthenticationPage.AliasInputBox");
    private static final By REGISTER_BUTTON = get("AuthenticationPage.RegisterButton");
    private static final By STATE_LIST = get("AuthenticationPage.StateList");
    private static final By USA_COUNTRY = get("AuthenticationPage.USACountry");
    private static final By LOG_OUT_BUTTON = get("AuthenticationPage.LogOutButton");

    private static final String MONTH = "#months>option:nth-child(2)";
    private static final String YEAR = "#years>option:nth-child(2)";


    public static void selectMr(){
        $(MR_RADIO_BUTTON).waitUntil(Condition.appear, 1000).click();
    }

    public static void selectMrs(){
        $(MRS_RADIO_BOTTON).waitUntil(Condition.appear, 1000).click();
    }

    public static void fillFirstName(String firstName){
        $(FIRST_NAME_INPUT_BOX).setValue(firstName);
    }

    public static void fillLastName(String lastName){
        $(LAST_NAME_INPUT_BOX).setValue(lastName);
    }

    public static void setPassword(String password){
        $(PASSWORD_INPUT_BOX).setValue(password);
    }

    public static void setDayOfBirth(String dayOfBirth){
        $(DAY_OF_BIRTH_INPUT_BOX).setValue(dayOfBirth);
    }

    public static void setMonthOfBirth(int monthOfBirth){
        int childNumber = monthOfBirth + 1;
        $(MONTH_OF_BIRTH_INPUT_BOX).click();
        $(By.cssSelector(MONTH.replace("2", "" +monthOfBirth))).waitUntil(Condition.appear, 10000).click();
    }

    public static void setYearOfBirth(int yearOfBirth){
        int childNumber = 2018 - yearOfBirth;
        $(YEAR_OF_BIRTH_INPUT_BOX).click();
        $(By.cssSelector(YEAR.replace("2", "" + childNumber))).waitUntil(Condition.appear, 10000).click();
    }

    public static void setSigUpForNewsletter(){
        $(NEWSLETTER_CHECK_BOX).click();
    }

    public static void setSingUpForSpecialOffer(){
        $(SPECIAL_OFFER_CHECK_BOX).click();
    }

    public static void setAddressFirstLine(String addressFirstLine){
        $(ADDRESS_FIRST_LINE_INPUT_BOX).setValue(addressFirstLine);
    }

    public static void setAddressSecondLine(String addressSecondLine){
        $(ADDRESS_SECOND_LINE_INPUT_BOX).setValue(addressSecondLine);
    }

    public static void setAddressFirstName(String addressFirstName){
        $(ADDRESS_FIRST_LINE_INPUT_BOX).setValue(addressFirstName);
    }

    public static void setAddressLastName(String addressLastName){
        $(ADDRESS_LAST_NAME_INPUT_BOX).setValue(addressLastName);
    }

    public static void setCompanyName(String companyName){
        $(COMPANY_NAME_INPUT_BOX).setValue(companyName);
    }

    public static void setCity(String city){
        $(CITY_INPUT_BOX).setValue(city);
    }

    public static void setState(String state){
        $(STATE_INPUT_BOX).click();

        for (SelenideElement se: $$(STATE_LIST)) {
            if (se.getText().contentEquals(state)){
                se.click();
            }
        }
    }

    public static void setPostCode(String postCode){
        $(POST_CODE_INPUT_BOX).setValue(postCode);
    }

    public static void setCountry(){
        $(COUNTRY_INPUT_BOX).click();
        $(USA_COUNTRY).click();
    }

    public static void setAdditionalInformation(String additionalInformation){
        $(ADDITIONAL_INFORMATION_INPUT_BOX).setValue(additionalInformation);
    }

    public static void setHomePhone(String homePhone){
        $(HOME_PHONE_INPUT_BOX).setValue(homePhone);
    }

    public static void setMobilePhone(String mobilePhone){
        $(MOBILE_PHONE_INPUT_BOX).setValue(mobilePhone);
    }

    public static void setAlias(String alias){
        $(ALIAS_INPUT_BOX).setValue(alias);
    }

    public static void register(){
        $(REGISTER_BUTTON).click();
    }

    public static boolean logoutButtonExists(){
        return $(LOG_OUT_BUTTON).waitUntil(Condition.appear, 10000).isDisplayed();
    }

}
