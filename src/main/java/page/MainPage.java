package page;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import com.codeborne.selenide.WebDriverRunner;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;
import static com.codeborne.selenide.Selenide.sleep;
import static helpers.Locators.get;

/**
 * Created by User on 06.09.2016.
 */
public class MainPage {
    private static final By CONTACT_US_BUTTON = get("HomePage.ContactUsButton");
    private static final By SIGN_IN_BUTTON = get("HomePage.SingInButton");
    private static final By SEARCH_INPUT_BOX = get("HomePage.SearchInputBox");
    private static final By SEARCH_BUTTON = get("HomePage.SearchButton");
    private static final By PRODUCT_COUNT_HEADING = get("HomePage.ProductCountHeading");
    private static final By PRODUCT_ITEM_CONTAINER = get("HomePage.ProductItemContainer");
    private static final By ADD_TO_CART_BUTTON = get("HomePage.AddToCartButton");
    private static final By ADD_TO_CART_SPAN=get("HomePage.AddToCartSpan");
    private static final By PICTURES_OF_ITEM = get("HomePage.PicturesOfItem");
    private static final By CART_BUTTON = get("HomePage.CartButton");
    private static final By PICTURE_AT_THE_CART = get("HomePage.PictureAtCart");
    private static final By TRASH_ICON = get("HomePage.TrashIcon");
    private static final By NO_ITEM_LABEL = get("HomePage.NoItemLabel");

    public static void goToContactPage(){
        $(CONTACT_US_BUTTON).click();
    }

    public static void goToSignInPage(){
        $(SIGN_IN_BUTTON).click();
    }

    public static void fillSearchInputBox(String searchItem){
        $(SEARCH_INPUT_BOX).setValue(searchItem);
    }

    public static void clickSearchButton(){
        $(SEARCH_BUTTON).click();
    }

    public static boolean successInSearch(){
        return $(PRODUCT_COUNT_HEADING).waitUntil(Condition.appear, 1000).isDisplayed();
    }

    public static String addToTheCart(int numberOfItem)  {
        int counter = 0;
        for (SelenideElement se:$$(PICTURES_OF_ITEM)) {
            if(se.isDisplayed()) {
                counter++;
                if (counter == numberOfItem) {
                    String pictureSource = se.getAttribute("src");
                    se.click();
                    $(ADD_TO_CART_BUTTON).click();
                    sleep(500);
                    return "" + pictureSource;

                }
            }
        }
        return "";
    }

    public static String getPictureSourceFromTheCart(){
        $(CART_BUTTON).waitUntil(Condition.appear, 10000).click();
        return $(PICTURE_AT_THE_CART).getAttribute("src");
    }

    public static void deleteItemFromCart(){
        $(TRASH_ICON).click();
    }

    public static boolean cartIsEmpty(){
        return $(NO_ITEM_LABEL).waitUntil(Condition.appear, 10000).isDisplayed();
    }
}
