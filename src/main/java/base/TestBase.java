package base;

import com.codeborne.selenide.Configuration;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;

import static com.codeborne.selenide.Selenide.close;
import static com.codeborne.selenide.Selenide.open;

public class TestBase {
    private static final String URL = "http://automationpractice.com/index.php";
    private static final int IMPLICIT_WAITER = 10000;
    protected static final String DEFAULT_BROWSER = "firefox";

    @BeforeTest(alwaysRun = true)
    protected void oneTimeSetup() {
        Configuration.timeout = IMPLICIT_WAITER;
        Configuration.baseUrl = URL;
        Configuration.browser = DEFAULT_BROWSER;
        open(URL);
    }

    @AfterMethod(alwaysRun = true)
    protected void tearDown() {
        close();
    }

}
