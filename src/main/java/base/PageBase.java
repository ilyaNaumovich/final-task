package base;

import com.codeborne.selenide.Condition;
import helpers.Waiter;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;

import static com.codeborne.selenide.Selenide.$;

public class PageBase {

    protected static void titleShouldAppear(String expectedTitle) {
        Waiter.getWaiter().until(ExpectedConditions.titleIs(expectedTitle));
    }

    protected static void shouldAppear(By... expectedElements) {
        for (By element : expectedElements) {
            $(element).shouldBe(Condition.visible);
        }
    }
}
