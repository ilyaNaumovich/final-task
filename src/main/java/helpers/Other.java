package helpers;

import java.util.Random;

/**
 * Created by User on 12.09.2016.
 */
public class Other {
    private static final String mCHAR = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
    static Random random = new Random();

    public static String createRandomString(int strLength) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < strLength; i++) {
            int number = random.nextInt(mCHAR.length());
            char ch = mCHAR.charAt(number);
            builder.append(ch);
        }
        return builder.toString();
    }

}
