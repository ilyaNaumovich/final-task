import base.TestBase;
import org.sikuli.script.FindFailed;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.asserts.Assertion;
import page.AuthenticationPage;
import page.ContactUsPage;
import page.LoginPage;
import page.MainPage;

import java.awt.*;

public class UITests extends TestBase {

    private final String EMAIL_ADDRESS = "smth@tut.by";
    private final String TEXT_FILE_PATH = "\\src\\main\\resources\\testFile.txt";
    private final String ORDER_REFERENCE = "test order reference";
    private final String MESSAGE = "Hello!";
    private final String FIRST_NAME = "Ivan";
    private final String LAST_NAME = "Ivanov";
    private final String PASSWORD = "Ivan123";
    private final String DAY_OF_BIRTH = "12";
    private final int MONTH_OF_BIRTH = 10;
    private final int YEAR_OF_BIRTH = 1985;
    private final String COMPANY_NAME = "DPI solution";
    private final String ADDRESS_FIRST_LINE = "220119 Minsk district";
    private final String ADDRESS_SECOND_LINE = "Beletskogo street, 25";
    private final String CITY = "Slutsk";
    private final String STATE = "Hawaii";
    private final String POST_CODE = "12345";
    private final String ADDITIONAL_INFORMATION = "Test additional information";
    private final String HOME_PHONE = "222-322";
    private final String MOBILE_PHONE = "8- (029) - 222-322";
    private final String ALIAS = "Val";
    private final String SEARCH_ITEM = "Blouse";
    private final int PICTURE_NUMBER = 3;


    @Test
    public void contactUsFormSentSuccessfullyTest() throws AWTException, FindFailed, InterruptedException {
        MainPage.goToContactPage();
        ContactUsPage.choseCustomerServiceSubject();
        ContactUsPage.fillEmailAddress(EMAIL_ADDRESS);
        ContactUsPage.attachFile(System.getProperty("user.dir") + TEXT_FILE_PATH);
        ContactUsPage.fillOrderReference(ORDER_REFERENCE);
        ContactUsPage.fillMessage(MESSAGE);
        ContactUsPage.sendMessage();

        Assert.assertTrue(ContactUsPage.successFileSentMessageIsVisible());
    }

    @Test
    public void contactUsErrorMessageAppearTest() throws AWTException, FindFailed, InterruptedException {
        MainPage.goToContactPage();
        ContactUsPage.choseWebmasterSubject();
        ContactUsPage.fillEmailAddress(EMAIL_ADDRESS);
        ContactUsPage.attachFile(System.getProperty("user.dir") + TEXT_FILE_PATH);
        ContactUsPage.fillOrderReference(ORDER_REFERENCE);
        ContactUsPage.sendMessage();

        Assert.assertEquals(ContactUsPage.getErrorText(), "The message cannot be blank.");
    }

    @Test
    public void abilityToRegisterTest() {
        MainPage.goToSignInPage();
        LoginPage.fillEmailAddress(EMAIL_ADDRESS);
        LoginPage.createAccount();
        AuthenticationPage.fillFirstName(FIRST_NAME);
        AuthenticationPage.fillLastName(LAST_NAME);
        AuthenticationPage.setPassword(PASSWORD);
        AuthenticationPage.setDayOfBirth(DAY_OF_BIRTH);
        AuthenticationPage.setMonthOfBirth(MONTH_OF_BIRTH);
        AuthenticationPage.setYearOfBirth(YEAR_OF_BIRTH);
        AuthenticationPage.setSigUpForNewsletter();
        AuthenticationPage.setSingUpForSpecialOffer();
        AuthenticationPage.setAddressFirstName(FIRST_NAME);
        AuthenticationPage.setAddressLastName(LAST_NAME);
        AuthenticationPage.setCompanyName(COMPANY_NAME);
        AuthenticationPage.setAddressFirstLine(ADDRESS_FIRST_LINE);
        AuthenticationPage.setAddressSecondLine(ADDRESS_SECOND_LINE);
        AuthenticationPage.setCity(CITY);
        AuthenticationPage.setState(STATE);
        AuthenticationPage.setPostCode(POST_CODE);
        AuthenticationPage.setCountry();
        AuthenticationPage.setAdditionalInformation(ADDITIONAL_INFORMATION);
        AuthenticationPage.setHomePhone(HOME_PHONE);
        AuthenticationPage.setMobilePhone(MOBILE_PHONE);
        AuthenticationPage.setAlias(ALIAS);
        AuthenticationPage.register();

        Assert.assertTrue(AuthenticationPage.logoutButtonExists());
    }

    @Test
    public void abilityToSearchItem() {
        MainPage.fillSearchInputBox(SEARCH_ITEM);
        MainPage.clickSearchButton();

        Assert.assertTrue(MainPage.successInSearch());
    }

    @Test
    public void abilityToAddItemsToCart() throws InterruptedException {
        String addedPictureSource = MainPage.addToTheCart(PICTURE_NUMBER);
        String cartPictureSource = MainPage.getPictureSourceFromTheCart();
        String modifyCartPicture = cartPictureSource.replace("small", "home");

        Assert.assertTrue(addedPictureSource.contentEquals(modifyCartPicture));
    }

    @Test
    public void abilityToDeleteItemsFromCart() throws InterruptedException {
        String addedPictureSource = MainPage.addToTheCart(PICTURE_NUMBER);
        String cartPictureSource = MainPage.getPictureSourceFromTheCart();

        MainPage.deleteItemFromCart();

        Assert.assertTrue(MainPage.cartIsEmpty());
    }



}
